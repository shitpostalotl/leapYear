#include <iostream>
#include "main.hpp"

bool years::check(int inyear) {
  return (inyear % 4 == 0 && !(inyear % 100 == 0 && inyear % 400 != 0));
}

int main() {
  std::cout << "input year: ";
  std::cin >> year;
  std::cout << years::check(year) << "\n";
}
